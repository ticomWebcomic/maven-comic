# Maven-Comic

Un ejemplo de como armar un comic usando css-grid para acomodar los paneles y agregar elementos de texto.

## Your Project

### ← README.md

That's this file, where you can tell people what your cool website does and how you built it.

### ← index.html

El indice donde ponemos las imagenes

### ← style.css

Estilo del comic

### ← grid.css

Estructura de la cuadricula del comic

### ← script.js

No se usan scripts

### ← assets

Drag in `assets`, like images or music, to add them to your project

## Made by [Glitch](https://glitch.com/)

\ ゜ o ゜)ノ
